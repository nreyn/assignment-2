import csv
__author__ = "### YOUR NAME HERE ###"
__maintainer__ = "Nick Reynolds"
__email__ = "nick.reynolds@mq.edu.au"


# Make sure you read the assignment specification for the specifics

# PART 1: Loading and Parsing a File

def read_stock_file(filename):
    """
    This function takes in a filename, such as ‘stocks.csv’, and returns the list of dictionaries. (i.e. csv.DictReader)
    """
    results = []
    # Open the file and get the results
    return results


def parse_stocks(stocks):
    """
    The purpose of this function is to change the stock data to be the right data types.
    """
    pass


# PART 2: Modifying a class

class Buffett:
    money = 0
    purchases = None

    def __init__(self, money):
        """ Don't change this function """
        self.money = money
        self.purchases = {}

    def buy(self, company, quantity, price):
        """
        This function handles Buffett buying a stock.
        """
        pass

    def how_many_can_i_sell(self, company, potential_sell_quantity):
        """
        Determine how many of a stock buffett can sell, i.e. check the quantity for that company.
        """
        return 0

    def sell(self, company, quantity, price):
        """
        Sell the stock if we can.
        """
        pass


# PART 3: Conditional rules


def open_market(money, stocks):
    """
    Here we are going to process our stocks according to the rules in the assignment specification.
    """
    warren = Buffett(money)
    for stock in stocks:
        company = stock['Company']
        price = stock['Price']
        quantity = stock['Units Available']

        # You will want to change the next two lines
        # Add the logic for deciding what to do i.e. check the company and decide if you want to buy or sell
        warren.buy(company, quantity, price)
        warren.sell(company, quantity, price)
        #

    return warren  # Don't change this


if __name__ == '__main__':
    # This is the part of the code that runs if you just press run
    # This is where you can print out what you want and test things yourself
    s = read_stock_file('stocks.csv')
    print('Stocks!', s)
    parse_stocks(s)
    result = open_market(2000, s)
    print(result.money)
    print(result.purchases)