import unittest
import A02 as lib

__author__ = "Nick Reynolds"
__maintainer__ = "Nick Reynolds"
__email__ = "nick.reynolds@mq.edu.au"


class Test_Section1(unittest.TestCase):
    def test_read_stock_file(self):
        results = lib.read_stock_file('stocks.csv')
        self.assertTrue(results is not None, 'Are you missing the stocks file? Or you just havent written the read yet')
        self.assertTrue(len(results) > 0, 'Have you modified the stocks file?')
        self.assertTrue(isinstance(results[0], dict), 'Looks like you arent reading dictionaries in, use the csv.reader')

    def test_parse_stocks(self):
        stocks = [{'Price': '$19.29', 'Date': '2017-06-01', 'Units Available': '4', 'Company': 'CCC'}]
        result = lib.parse_stocks(stocks)
        if result:
            stocks = result
        self.assertTrue(len(stocks) == 1,
                        'Somehow youre missing stock rows, you shouldnt need to delete elements from the list')
        result = stocks[0]
        print(result)
        self.assertTrue('Price' in result,
                        "You must have removed or renamed a dictionary key, dont do that")
        self.assertTrue('Date' in result,
                        "You must have removed or renamed a dictionary key, dont do that")
        self.assertTrue('Units Available' in result,
                        "You must have removed or renamed a dictionary key, dont do that")
        self.assertTrue('Company' in result,
                        "You must have removed or renamed a dictionary key, dont do that")
        self.assertEqual(result['Units Available'], 4,
                         "You are not correctly converting the Units Available into a int")
        self.assertEqual(result['Price'], 19.29,
                         "You are not correctly converting the price into a float")


class Test_Buffett(unittest.TestCase):
    def test_init(self):
        warren = lib.Buffett(200)
        message = 'I should be able to make new warrens with x dollars. Did you accidently change the constructor?'
        self.assertEqual(warren.money, 200, message)
        warren = lib.Buffett(999)
        self.assertEqual(warren.money, 999, message)

    def test_buy(self):
        warren = lib.Buffett(200)
        warren.buy('Company A', 5, 10)
        self.assertEqual(warren.money, 150, 'You are not subtracting money when warren buys a stock')
        warren = lib.Buffett(10)
        warren.buy('Company B', 4, 6)
        self.assertEqual(warren.money, 10, 'You are not checking if he has enough money to buy the stock')
        self.assertEqual(warren.purchases, dict(),
                         'There should be no purchase if there is not enough money')
        warren = lib.Buffett(200)
        warren.buy('Company C', 5, 10)
        expected = {
            'Company C': 5
        }
        self.assertEqual(warren.purchases, expected,
                         'You are not adding the stock and quantity to the purchases dictionary')
        warren.buy('Company C', 2, 10)
        expected = {
            'Company C': 7
        }
        self.assertEqual(warren.purchases, expected,
                         'You are not incrementing the count of stock purchases for a company')
        warren.buy('Company A', 2, 10)
        expected = {
            'Company C': 7,
            'Company A': 2
        }
        self.assertEqual(warren.purchases, expected,
                         'You are not handling multiple companies stocks correctly')

    def test_how_many_can_i_sell(self):
        warren = lib.Buffett(200)
        warren.buy('Company A', 5, 10)
        self.assertEqual(warren.how_many_can_i_sell('Company A', 5), 5, 'If Buffett has 5 stocks of AAA, he should be able to sell 5 stocks of AAA.')
        self.assertEqual(warren.how_many_can_i_sell('Company A', 6), 5,
                         'If Buffett has 5 stocks, he can still sell 5 stocks. Youre not checking the potential quantity correctly')
        self.assertEqual(warren.how_many_can_i_sell('Company A', 3), 3,
                         'If Buffett has 5 stocks, he can still sell 3 stocks. Youre not checking the potential quantity correctly')
        self.assertEqual(warren.how_many_can_i_sell('Company B', 3), 0,
                         'If Buffett has no stocks in a company he cant sell anything')

    def test_sell(self):
        warren = lib.Buffett(200)
        warren.buy('Company A', 5, 10)
        warren.buy('Company B', 6, 10)
        warren.sell('Company A', 2, 20)
        self.assertEqual(warren.money, 130,
                         'Youre not correctly adding to their money when they sell')
        expected = {
            'Company A': 3,
            'Company B': 6
        }
        self.assertEqual(warren.purchases, expected,
                         'Youre not changing the companies quantity when they sell')
        warren.sell('Company B', 2, 20)
        expected = {
            'Company A': 3,
            'Company B': 4
        }
        self.assertEqual(warren.purchases, expected,
                         'Youre not changing the companies quantity when they sell')


class Test_Section3(unittest.TestCase):
    def test_returns_buffett(self):
        self.assertEqual(lib.open_market(100, []).__class__.__name__, lib.Buffett(100).__class__.__name__,
                         "Doesnt look like you're returning a buffett class from the open market!")

    def test_AAA(self):
        stocks = [{'Company': 'AAA', 'Price': '$1', 'Units Available': '5', 'Date': '2001-01-01'},
                  {'Company': 'AAA', 'Price': '$40', 'Units Available': '3', 'Date': '2001-01-01'},
                  {'Company': 'AAA', 'Price': '$19', 'Units Available': '6', 'Date': '2001-01-01'},
                  {'Company': 'AAA', 'Price': '$10', 'Units Available': '3', 'Date': '2001-01-01'},
                  {'Company': 'AAA', 'Price': '$5', 'Units Available': '8', 'Date': '2001-01-01'},
                  {'Company': 'AAA', 'Price': '$99', 'Units Available': '1', 'Date': '2001-01-01'}]
        result = lib.parse_stocks(stocks)
        if result:
            stocks = result
        results = lib.open_market(2000, stocks)
        self.assertEqual(results.purchases, {'AAA': 18})
        self.assertEqual(results.money, 2030)



if __name__ == '__main__':
    unittest.main(verbosity=2)